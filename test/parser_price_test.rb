require './bootstrap.rb'
cases = [
    {
        :uid => 'oid_6065941_6065941',
        :text => 'Auction $900,000 - $1 million',
        :lower_price => 900000.0,
        :middle_price => nil,
        :upper_price => 1000000.0,
    },
    {
        :uid => 'oid_6173299_6173299',
        :text => 'Auction QUOTING $520K+',
        :lower_price => 520000.0,
        :middle_price => nil,
        :upper_price => nil,
    },
    {
        :uid => 'oid_6173299_6173299',
        :text => 'Auction $1.9m+',
        :lower_price => 1900000.0,
        :middle_price => nil,
        :upper_price => nil,
    },
    {
        :uid => 'oid_6168393_6168393',
        :text => 'Auction more than $720k',
        :lower_price => 720000.0,
        :middle_price => nil,
        :upper_price => nil,
    },
    {
        :uid => 'oid_6128335_6128335',
        :text => 'Auction Bidding Guide: From $1.28M',
        :lower_price => 1280000.0,
        :middle_price => nil,
        :upper_price => nil,
    },
    {
		:uid => 'oid_6150249_6150249',
		:text => 'Auction $970K -  $1.05M',
		:lower_price => 970000.0,
		:middle_price => nil,
		:upper_price => 1050000.0,
	},
	{
		:uid => 'oid_6151593_6151593',
		:text => 'Auction $1.32m - $1.38m',
		:lower_price => 1320000.0,
		:middle_price => nil,
		:upper_price => 1380000.0,
	},
	{
		:uid => 'oid_6150393_6150393',
		:text => 'Auction $1.32M - $1.38M',
		:lower_price => 1320000.0,
		:middle_price => nil,
		:upper_price => 1380000.0,
	},
	{
		:uid => '',
		:text => 'Auction $620,000 - $680,000',
		:lower_price => 620000.0,
		:middle_price => nil,
		:upper_price => 680000.0,
	},
	{
		:uid => '',
		:text => 'Auction $600 - $660,000',
		:lower_price => 600000.0,
		:middle_price => nil,
		:upper_price => 660000.0,
	},
	{
		:uid => '',
		:text => 'Auction $635K - $685K',
		:lower_price => 635000.0,
		:middle_price => nil,
		:upper_price => 685000.0,
	},
	{
		:uid => 'oid_6104933_6104933',
		:text => 'Auction $1 - $1.1 million',
		:lower_price => 1000000.0,
		:middle_price => nil,
		:upper_price => 1100000.0,
	},
	{
		:uid => 'oid_6104725_6104725',
		:text => 'Auction $1.25 - $1.35 million',
		:lower_price => 1250000.0,
		:middle_price => nil,
		:upper_price => 1350000.0,
	},
	{
		:uid => 'oid_6104543_6104543',
		:text => 'Auction $2.2 - $2.4 million',
		:lower_price => 2200000.0,
		:middle_price => nil,
		:upper_price => 2400000.0,
	},
	{
		:uid => 'oid_6156397_6156397',
		:text => 'Auction $790k to $845k',
		:lower_price => 790000.0,
		:middle_price => nil,
		:upper_price => 845000.0,
	},
	{
		:uid => 'oid_6155601_6155601',
		:text => 'Auction $520K - $570K',
		:lower_price => 520000.0,
		:middle_price => nil,
		:upper_price => 570000.0,
	},
	{
		:uid => 'oid_6151593_6151593',
		:text => 'Auction $1.32 - $1.38 million',
		:lower_price => 1320000.0,
		:middle_price => nil,
		:upper_price => 1380000.0,
	},
]
cases.each { |e|
	parser = ParserPrice.new
	lower_price, middle_price, upper_price, price_match = parser.parse(e[:text])
	status = true;
	status = status && (lower_price == e[:lower_price] || (lower_price.nil? && e[:lower_price].nil?))
	status = status && (middle_price == e[:middle_price] || (middle_price.nil? && e[:middle_price].nil?))
	status = status && (upper_price == e[:upper_price] || (upper_price.nil? && e[:upper_price].nil?))
	if !status
		puts e[:uid]
		puts e[:text]
		puts [lower_price, middle_price, upper_price, price_match].join(' - ')
	end
}
