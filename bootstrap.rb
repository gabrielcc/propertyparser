require 'yaml'
require 'active_record'

DATABASE_ENV = ENV['DATABASE_ENV'] || 'development'
config = YAML.load_file('./config/database.yml')[DATABASE_ENV]
ActiveRecord::Base.establish_connection config

Dir["./src/*.rb"].each {|file| require file }
Dir["./src/models/*.rb"].each {|file| require file }
