class ParserAddress
	@@regexp = /(?<number>.*?) (?<street>[A-Z].*?) (?<city>[A-Z]{2}[A-Z ]*) (?<postal_code>[0-9]{2,6})/

	def parse(address)
		number, street, city, postal_code = nil
		match = @@regexp.match(address)
		if !match.nil?
			number = match[:number] unless match[:number].nil?
			street = match[:street] unless match[:street].nil?
			city = match[:city] unless match[:city].nil?
			postal_code = match[:postal_code] unless match[:postal_code].nil?
		end
		[number, street, city, postal_code]
	end
end