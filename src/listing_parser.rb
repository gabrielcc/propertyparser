require 'nokogiri'
require 'open-uri'
require 'pry'

class ListingParser
    @@locations = ['New South Wales', 'Victoria']
    def initialize
        @start = Time.now
    end
    def parse
        @@locations.each{ |term|
            parse_location(term)
        }
    end

	def parse_location(term)
        1.upto(34).each { |page|
        	puts "Parsing #{term} #{page}"
        	puts "-------------------"
        	parse_document(term, page) do |y|
        		if y.price_match
        			listing = Listing.find_or_initialize_by(uid: y.uid)
        			listing.update_attributes(y.to_hash)
        			listing.parsing_date = @start
        			listing.save
        		else
        			puts y.to_s
        		end
        	end
        }
	end
	def parse_document(location, page)
		results = extract_results(location, page)
		results.css('div.module-listing').each do |result|
			yield parse_individual_result(result)
		end
	end

	def parse_individual_result(result)
		listing_result = ListingResult.new
		ParserConfig.elements.each do |element|
			listing_result.send("#{element}=".intern, result.css(ParserConfig.get(element)).text)
		end
		listing_result.uid = result.attribute('id').to_s
		agent = result.css('.property-client-link')
		if agent.count() > 0
			listing_result.agent = agent.attribute('title').to_s
		end
		listing_result
	end
	def extract_results(location, page)
		file = get_file_content(location, page)
		doc = Nokogiri::HTML(file)
		doc.css("#results")
	end

	def get_file_content(location, page)
		data = ''
		open(ParserConfig.get_listing_uri(location, page)) { |io|
			data = io.read
		}
		data
	end
end
