class ParserConfig
	@@config = {
	'address' => 'h2.address-text a',
	'price' => 'div.sale-text',
	'beds' => 'div.icons_bed',
	'baths' => 'div.icons_bath',
	'cars' => 'div.icons_car',
	'auction_date' => 'div.property_details  span'
	}
	def self.elements
		@@config.keys
	end

	def self.get(element)
		@@config[element]
	end

	def self.get_listing_uri(term, page)
	    term = URI::encode(term)
		uri = "http://www.realestateview.com.au/portal/search?rm=search&journeyplanner=0&jpwkend=0&jpday=1&jpdate=1&jpstime=0&jpetime=0&P=1&ptr=r&con=S&portalview=residential&portalsection=buy&bs=12&sub=#{term}&pt=hou&pt=uni&pt=tow&pt=vil&pt=lan&pt=dev&prl=0&prh=0&bel=0&beh=0&bal=0&bah=0&pal=0&pah=0&exsale=1&pg=#{page}"
        uri
	end
end