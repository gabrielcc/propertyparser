require 'chronic'

class ListingResult
  attr_accessor  :address_number, :address_street, :address_city, :addres_postcode, :beds, :baths, :cars, :auction_date
  attr_accessor :price_match, :uid, :agent
  attr_accessor :lower_price, :middle_price, :upper_price
  attr_accessor :price

  @@ignore = ['price_parser', 'address_parser', 'price_match']
  
  def get_price_parser
    @price_parser = ParserPrice.new if @price_parser.nil?
    @price_parser
  end

  def get_address_parser
    @address_parser = ParserAddress.new if @address_parser.nil?
    @address_parser
  end

  def agent=(agent)
    @agent = agent.gsub(/Link to /, '').gsub(' listings', '')
  end

  def price=(price)
    @price = price.strip
    @lower_price, @middle_price, @upper_price, @price_match = get_price_parser.parse(price)
  end


  def address=(address)
    @address_number, @address_street, @address_city, @addres_postcode = get_address_parser.parse(address)
  end

  def beds=(beds)
  	@beds = to_number(beds)
  end

  def baths=(baths)
  	@baths = to_number(baths)
  end

  def cars=(cars)
  	@cars = to_number(cars)
  
  end

  def auction_date=(auction_date)
    @auction_date = Chronic.parse(auction_date) 
  end
  
  def to_number(value)
  	value.gsub(/[^0-9]/,'').to_i
  end

  def to_s
   "Entry id: #{@uid}
Address: #{@address_number} #{@address_street} #{@address_city} #{@addres_postcode}
Price: low #{@lower_price} middle #{@middle_price} upper #{@upper_price}
Price Text: #{@price}
Beds #{@beds} Baths #{@baths} Cars #{@cars}
Auction Date #{@auction_date}
Agent #{@agent}
   " 
  end

  def to_hash
    hash = {}
    instance_variables.each {|var|
      hash_key = var.to_s.delete("@")
      hash[hash_key] = instance_variable_get(var) if !@@ignore.include? hash_key
    }
    hash
  end

end