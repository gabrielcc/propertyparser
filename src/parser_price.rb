class ParserPrice
	@@config = [
		{
			# Auction $900,000 - $1 million
			:regexp => /^.*?\$(?<lower_price>[0-9,]{5,9}).*?\$(?<upper_price>[0-9.]*) mill.*$/,
			:factor => {
				:lower => 1,
				:middle => 1000000,
				:upper => 1000000
			}
		},
		{
			# Auction $970K -  $1.05M
			:regexp => /^.*?\$(?<lower_price>[0-9.]*)[k|K].*?\$(?<upper_price>[0-9.]*)[m|M]$/,
			:factor => {
				:lower => 1000,
				:middle => 1000000,
				:upper => 1000000
			}
		},
	    {
			# Auction $970K -  $1.05M
			:regexp => /^.*?\$(?<lower_price>[0-9.]*)[k|K].*?\$(?<upper_price>[0-9.]*)[m|M]$/,
			:factor => {
				:lower => 1000,
				:middle => 1000000,
				:upper => 1000000
			}
		},
		{
			# Auction $1.32m - $1.38m
			:regexp => /^.*?\$(?<lower_price>[0-9.]*)[m|M].*?\$(?<upper_price>[0-9.]*)[m|M]$/,
			:factor => {
				:lower => 1000000,
				:middle => 1000000,
				:upper => 1000000
			}
		},
        {
	        #Auction Bidding Guide: From $1.28M
	        :regexp => /^.*?\$(?<lower_price>[0-9.]*)[M|m].*$/,
			:factor => {
				:lower => 1000000,
				:middle => 1000000,
				:upper => 1000000
			}
	    },
	   
		{
			# Auction $1 - $1.1 million
			:regexp => /^.*?\$(?<lower_price>[0-9.]*).*?\$(?<upper_price>[0-9.]*) million$/,
			:factor => {
				:lower => 1000000,
				:middle => 1000000,
				:upper => 1000000
			}
		},
		{
			# Auction $1 - $1.1 million
			:regexp => /^.*?\$(?<lower_price>[0-9.]*) million$/,
			:factor => {
				:lower => 1000000,
				:middle => 1000000,
				:upper => 1000000
			}
		},
		{
			# Auction $600 - $660,000
			:regexp => /^.*?\$(?<lower_price>[0-9]{3}).*?\$(?<upper_price>[0-9,]*)$/,
			:factor => {
				:lower => 1000,
				:middle => 1,
				:upper => 1
			}
		},
		{
			# Auction $635K - $685K
			#Auction $790k to $845k
			:regexp => /^.*?\$(?<lower_price>[0-9]{1,4})[K|k].*?\$(?<upper_price>[0-9,]{1,4})[K|k]$/,
			:factor => {
				:lower => 1000,
				:middle => 1,
				:upper => 1000
			}
		},
		 {
	        #Auction Bidding Guide: From $720k
	        :regexp => /^.*?\$(?<lower_price>[0-9.]*)[k].*$/,
			:factor => {
				:lower => 1000,
				:middle => 1000,
				:upper => 1000
			}
	    },
		{
			:regexp => /^.*?\$(?<lower_price>[0-9,]*).*?\$(?<upper_price>[0-9,]*)$/,
			:factor => {
				:lower => 1,
				:middle => 1,
				:upper => 1
			}
		},
		{
			:regexp => /^.*?\$(?<lower_price>[0-9,]*).*?$/,
			:factor => {
				:lower => 1,
				:middle => 1,
				:upper => 1
			}
		},

	]

	def parse(price)
		price = price.downcase
		lower_price = middle_price = upper_price = nil
		price_match = false
		@@config.each { |parser|
			match = parser[:regexp].match(price)
			next if match.nil?
			lower_price = to_number(match['lower_price']) * parser[:factor][:lower]  if match.names.include? 'lower_price'
			upper_price = to_number(match['upper_price']) * parser[:factor][:upper] if match.names.include? 'upper_price'
			middle_price = to_number(match['middle_price']) * parser[:factor][:middle] if match.names.include? 'middle_price'
			price_match = true
			# puts parser[:regexp]
			break
		}
		[lower_price, middle_price, upper_price, price_match]
	end

	def to_number(value)
  		value.gsub(/[^0-9.]/,'').to_f
  end
end
