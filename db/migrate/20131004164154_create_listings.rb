class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
    t.string :uid, :null => false
	t.string :address_number, :null => false
	t.string :address_street, :null => false
	t.string :address_city, :null => false
	t.string :addres_postcode, :null => false
	t.integer :beds
	t.integer :baths
	t.integer :cars
	t.date :auction_date, :null => false
	t.string :agent, :null => true
	t.string :price, :null => false
	t.float :lower_price, :null => true
	t.float :middle_price, :null => true
	t.float :upper_price, :null => true
	t.date :parsing_date
	t.timestamps
    end
    add_index(:listings, :uid, :unique => true)
  end
end
