class AddUniqueIndexToListings < ActiveRecord::Migration
  def change
    remove_index(:listings, :uid)
   	add_index(:listings, [:uid, :price], :unique => true)
  end
end
